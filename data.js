const app_id = "fb693631";
const app_key = "33de0218cda1e6d638683317dabebf5f";

export async function fetchRecipes(searchText) {
	const response = await fetch(`https://api.edamam.com/search?q=${searchText}&app_id=${app_id}&app_key=${app_key}&to=8`)
	const data = await response.json();
	return data.hits.map((hit) => hit.recipe);
}

export async function fetchRecipesByMealType(searchText, mealType) {
	const response = await fetch(`https://api.edamam.com/search?q=${searchText}&app_id=${app_id}&app_key=${app_key}&mealType=${mealType}&to=8`)
	const data = await response.json();
	return data.hits.map((hit) => hit.recipe);
}

export async function fetchRecipesByDishType(searchText, dishType) {
	const response = await fetch(`https://api.edamam.com/search?q=${searchText}&app_id=${app_id}&app_key=${app_key}&dishType=${dishType}&to=8`)
	const data = await response.json();
	return data.hits.map((hit) => hit.recipe);
}

export async function fetchRecipesByCuisineType(searchText, cuisineType) {
	const response = await fetch(`https://api.edamam.com/search?q=${searchText}&app_id=${app_id}&app_key=${app_key}&cuisineType=${cuisineType}&to=8`)
	const data = await response.json();
	return data.hits.map((hit) => hit.recipe);
}
import { fetchRecipes } from "./data.js";
import { fetchRecipesByMealType } from "./data.js";
import { fetchRecipesByDishType } from "./data.js";
import { fetchRecipesByCuisineType } from "./data.js"; 
import { displayRecipes } from "./view.js";

const searchInput = document.getElementById("searchInput");
const searchButton = document.getElementById("searchButton");
const mealTypeButtons = document.querySelectorAll(".meal-type");
const dishTypeButtons = document.querySelectorAll(".dish-type");
const cuisineTypeButtons = document.querySelectorAll(".cuisine-type");

async function searchRecipes(fetchFunction) {
    searchButton.addEventListener("click", async (event) => {
        const searchText = searchInput.value;
        const selectedTypeButton = [...fetchFunction.buttons].find((button) => button.classList.contains("active"));
        const type = selectedTypeButton.dataset[fetchFunction.filter];
        const recipes = await fetchFunction.fetch(searchText, type);
        displayRecipes(recipes);
    });
}

function filterByType(typeButtons) {
    typeButtons.forEach((button) => {
        button.addEventListener("click", () => {
            typeButtons.forEach((button) => button.classList.remove("active"));
            button.classList.add("active");
        });
    });
}

searchRecipes({
    fetch: fetchRecipes, buttons: [], filter: "", });

searchRecipes({
    fetch: fetchRecipesByMealType, buttons: mealTypeButtons, filter: "mealType", });
filterByType(mealTypeButtons);

searchRecipes({
    fetch: fetchRecipesByDishType, buttons: dishTypeButtons, filter: "dishType", });
filterByType(dishTypeButtons);

searchRecipes({
    fetch: fetchRecipesByCuisineType, buttons: cuisineTypeButtons, filter: "cuisineType", });
filterByType(cuisineTypeButtons);

// Привязка обработчиков для кнопок фильтрации
//const mealTypeButton = document.getElementById("mealTypeButton");
//mealTypeButton.addEventListener("click", () => {
   // const mealTypeInput = document.getElementById("mealTypeInput").value;
    //filterByMealType(mealTypeInput);
//});

//const dishTypeButton = document.getElementById("dishTypeButton");
//dishTypeButton.addEventListener("click", () => {
   // const dishTypeInput = document.getElementById("dishTypeInput").value;
    //filterByDishType(dishTypeInput);
//});

//const cuisineTypeButton = document.getElementById("cuisineTypeButton");
//cuisineTypeButton.addEventListener("click", () => {
    //const cuisineTypeInput = document.getElementById("cuisineTypeInput").value;
    //filterByCuisineType(cuisineTypeInput);
//});
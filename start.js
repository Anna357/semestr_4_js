class Form {
  constructor() {
    this.form = document.createElement('form');
    this.askLabel = document.createElement('label');
    this.nameInput = document.createElement('input');
    this.buttonNext = document.createElement('button');
    this.form.classList.add('form');
    this.askLabel.classList.add('label');
    this.nameInput.classList.add('input');
    this.buttonNext.classList.add('button');
  }

  async createForm() {
    this.askLabel.htmlFor = 'name';
    this.nameInput.type = 'text';
    this.nameInput.id = 'name';
    this.nameInput.name = 'name';
    this.nameInput.placeholder = "Имя";
    this.buttonNext.id = 'Nextbutton';
    this.buttonNext.type = 'submit';
    this.buttonNext.innerText = 'Далее';
    this.form.append(this.askLabel, this.nameInput, this.buttonNext);
    document.body.append(this.form);

    this.form.addEventListener('submit', async (event) => {
      event.preventDefault();
      let name = this.nameInput.value;
      if (name === '') {
        alert('Введите имя!!!!');
      } else {
        await this.saveUsername(name);
        window.location.replace('main.html');
      }
    });
  }

  async saveUsername(name) {
    localStorage.setItem('username', name); // сохранение данных в браузере
  }

}

const form = new Form();
form.createForm();
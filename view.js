export function displayRecipes(recipes) {
	const searchResults = document.getElementById("searchResults");
	searchResults.innerHTML = "";

recipes.forEach((recipe) => {
	const recipeDiv = document.createElement("div");

const title = document.createElement("h3");
title.innerHTML = recipe.label;

const image = document.createElement("img");
image.src = recipe.image;

const ingredientsButton = document.createElement("button");
ingredientsButton.innerHTML = "Подробнее";
ingredientsButton.addEventListener("click", displayIngredients.bind(null, recipe));

const recipeDetails = document.createElement("div");
recipeDetails.appendChild(image);
recipeDetails.appendChild(ingredientsButton);

recipeDiv.appendChild(title);
recipeDiv.appendChild(recipeDetails);

searchResults.appendChild(recipeDiv);
}); }

function displayIngredients(recipe) {

// Отображение ингредиентов в модальном окне
	const modalDiv = document.createElement("div");
	modalDiv.id = "modal-background";
	const modalContent = document.createElement("div");
	modalContent.id = "modal-content";
	const modalTitle = document.createElement("h2");
	modalTitle.id = "modal-title";
	modalContent.appendChild(modalTitle);
	const modalIngredients = document.createElement("ul");
	modalIngredients.id = "modal-ingredients";
	modalContent.appendChild(modalIngredients);
	modalDiv.appendChild(modalContent);
	modalTitle.textContent = recipe.label;
	recipe.ingredients.forEach((ingredient) => {
		const ingredientListItem = document.createElement("li");
		ingredientListItem.textContent = ingredient.text;
		modalIngredients.appendChild(ingredientListItem);
	});
	document.documentElement.appendChild(modalDiv);

// Добавляем обработчик события клика на задний фон модального окна
	modalDiv.addEventListener("click", (event) => {
		if (event.target.id === "modal-background") {
			document.documentElement.removeChild(modalDiv);
		}
	});
}